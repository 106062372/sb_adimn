# -*- coding: utf-8 -*-
from flask import Flask, render_template
app = Flask(__name__)
#app._static_folder =r'C:\Users\USER\Desktop\前端\startbootstrap-sb-admin\dist'

users = [{"username": "USER_1", "url": "USER_1.com"},
         {"username": "USER_2", "url": "USER_2.com"},
         {"username": "USER_3", "url": "USER_3.com"},
         {"username": "USER_4", "url": "USER_4.com"},
         {"username": "USER_5", "url": "USER_5.com"},
         {"username": "USER_6", "url": "USER_6.com"},
         {"username": "USER_7", "url": "USER_7.com"},
         {"username": "USER_8", "url": "USER_8.com"},
         {"username": "USER_9", "url": "USER_9.com"},
         {"username": "USER_10", "url": "USER_10.com"},
         {"username": "USER_11", "url": "USER_11.com"},
         {"username": "USER_12", "url": "USER_12.com"},
         {"username": "USER_13", "url": "USER_13.com"},
         {"username": "USER_14", "url": "USER_14.com"},
         {"username": "USER_15", "url": "USER_15.com"},
         {"username": "USER_16", "url": "USER_16.com"},
         {"username": "USER_17", "url": "USER_17.com"},
         {"username": "USER_18", "url": "USER_18.com"},
         {"username": "USER_19", "url": "USER_19.com"},
         {"username": "USER_20", "url": "USER_20.com"},
         {"username": "USER_21", "url": "USER_21.com"},
         {"username": "USER_22", "url": "USER_22.com"},
         {"username": "USER_23", "url": "USER_23.com"},
         {"username": "USER_24", "url": "USER_24.com"},
         {"username": "USER_25", "url": "USER_25.com"},
         {"username": "USER_26", "url": "USER_26.com"}]



module ={
	"module_name": "test_add",
	"module_type": "Recon",
	"module_author": "Xiaobye",
	"module_description":"My first hello world module",
	"settings":[
			{
				"name":"file",
				"act": "upload",
				"file":"wordlist.txt"
			}
	]
}


results = {
  "module": "Git dump",
  "list": [
    {
      "target": "https://theeye.tw",
      "time": 42.46517205238342,
      "column_tag": [
        "normal",
        "state",
        "file"
      ],
      "columns": [
        "url",
        "state",
        "filename"
      ],
      "rows": [
        [
          "https://theeeeeye.tw",
          ["Danger","alert"],
          ["git_result.zip","http://127.0.0.1:5000/target/output/test/1604224801/Git%20dump/git_result.zip"]
        ],
        [
          "https://2.tw",
          ["2_1","good"],
          ["git_result.zip","http://127.0.0.1:5000/target/output/test/1604224801/Git%20dump/git_result.zip"]
        ]
        
      ],
      "files": [
        "git_result.zip"
      ]
    }
  ]
}


@app.route('/')
def jinja2_example():
    #return render_template("list_module.html", users=users)
    #return render_template("show_module.html", module=module)
    #return render_template("add_target.html")
    #return render_template("add_module.html")
    #return render_template("add_page.html", users=users)
    #return render_template("page.html")
    return render_template("result.html",results=results)
    
    
    


if __name__ == '__main__':
     app.run(host='127.0.0.1', port=8000)